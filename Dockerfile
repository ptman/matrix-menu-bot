# syntax=docker/dockerfile:1
# vim: set ft=dockerfile:
FROM golang:1.20-alpine3.18 AS buildgo
RUN apk add --no-cache ca-certificates make
COPY . ${GOPATH}/matrix-menu-bot
WORKDIR ${GOPATH}/matrix-menu-bot
RUN go get -v
RUN make matrix-menu-bot

FROM scratch
WORKDIR /
COPY --from=buildgo /etc/ssl /etc/ssl/
COPY --from=buildgo /go/matrix-menu-bot/matrix-menu-bot /

CMD ["/matrix-menu-bot"]
