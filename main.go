// Copyright (c) 2022 Paul Tötterman <paul.totterman@iki.fi>. All rights reserved.

package main

import (
	"fmt"
	"log"
	"os"
	"reflect"
	"strings"

	"github.com/pkg/errors"
	"gopkg.in/yaml.v3"
	"maunium.net/go/mautrix"
	"maunium.net/go/mautrix/event"
	"maunium.net/go/mautrix/id"
)

const (
	defaultConfigFileName = "config.yaml"
)

type config struct {
	AccessToken string    `yaml:"accessToken"`
	Admin       id.UserID `yaml:"admin"`
	hsURI       string
	Password    string `yaml:"password"`
	userID      id.UserID
	UserID      string `yaml:"userID"`
	username    string
}

type PollKind string

//nolint:deadcode
const (
	KindDisclosed   PollKind = "org.matrix.msc3381.poll.disclosed"
	KindUndisclosed PollKind = "org.matrix.msc3381.poll.undisclosed"
)

type PollQuestion struct {
	Text    string            `json:"org.matrix.msc1767.text,omitempty"`
	Body    string            `json:"body,omitempty"`
	MsgType event.MessageType `json:"msgtype,omitempty"`
}

type PollAnswer struct {
	ID   string `json:"id,omitempty"`
	Text string `json:"org.matrix.msc1767.text,omitempty"`
}

type PollStart struct {
	Question      PollQuestion `json:"question,omitempty"`
	Kind          PollKind     `json:"kind,omitempty"`
	MaxSelections int          `json:"max_selections,omitempty"`
	Answers       []PollAnswer `json:"answers"`
	FallbackText  string       `json:"org.matrix.msc1767.text,omitempty"`
}

type PollStartContent struct {
	PollStart PollStart `json:"org.matrix.msc3381.poll.start"`
}

type PollResponse struct {
	Answers []string `json:"answers,omitempty"`
}

type PollResponseContent struct {
	PollResponse PollResponse     `json:"org.matrix.msc3381.poll.response,omitempty"`
	RelatesTo    *event.RelatesTo `json:"m.relates_to,omitempty"`
}

type PollEndContent struct {
	PollEnd      map[string]string `json:"org.matrix.msc3381.poll.end"`
	FallbackText string            `json:"org.matrix.msc1767.text,omitempty"`
	Body         string            `json:"body,omitempty"`
	RelatesTo    *event.RelatesTo  `json:"m.relates_to"`
	MsgType      event.MessageType `json:"msgtype,omitempty"`
}

//nolint:gochecknoglobals,govet
var (
	EventPollStart = event.Type{
		"org.matrix.msc3381.poll.start",
		event.MessageEventType,
	}
	EventPollResponse = event.Type{
		"org.matrix.msc3381.poll.response",
		event.MessageEventType,
	}
	EventPollEnd = event.Type{
		"org.matrix.msc3381.poll.end",
		event.MessageEventType,
	}
)

func (c *config) parse() error {
	c.userID = id.UserID(c.UserID)

	local, hs, err := c.userID.ParseAndValidate()
	if err != nil {
		return errors.WithStack(err)
	}

	c.username = local

	wellknown, err := mautrix.DiscoverClientAPI(hs)
	if err != nil {
		return errors.WithStack(err)
	}

	c.hsURI = wellknown.Homeserver.BaseURL

	return nil
}

//nolint:gochecknoglobals
var (
	client *mautrix.Client
	conf   *config
	store  *mautrix.InMemoryStore
)

func processCommand(e *event.Event, fields []string, body string) {
	switch strings.ToLower(fields[0][1:]) {
	case "shutdown":
		os.Exit(0)
	case "leave":
		processLeave(e, fields[1:])
	case "menu":
		processMenu(e)
	default:
		_, err := client.SendNotice(e.RoomID, fmt.Sprintf(
			"unknown command: %s", fields[0]))
		errLog(err)
	}
}

func processLeave(e *event.Event, fields []string) {
	_, err := client.LeaveRoom(e.RoomID,
		&mautrix.ReqLeave{Reason: "asked to"})
	errLog(err)

	_, err = client.ForgetRoom(e.RoomID)
	errLog(err)
}

func processMenu(e *event.Event) {
	content := PollStartContent{
		PollStart: PollStart{
			Question: PollQuestion{
				Text:    "what do you want to manipulate?",
				Body:    "what do you want to manipulate?",
				MsgType: event.MsgText,
			},
			Kind:          KindDisclosed,
			MaxSelections: 1,
			Answers: []PollAnswer{
				{
					ID:   "door",
					Text: "door",
				},
				{
					ID:   "light",
					Text: "light",
				},
			},
		},
	}
	_, err := client.SendMessageEvent(e.RoomID, EventPollStart, content)
	errLog(err)
}

func readConfig() error {
	fn := defaultConfigFileName
	if len(os.Args) > 1 {
		fn = os.Args[1]
	}

	f, err := os.Open(fn)
	if err != nil {
		return errors.WithStack(err)
	}

	if err := yaml.NewDecoder(f).Decode(conf); err != nil {
		return errors.WithStack(err)
	}

	return nil
}

func init() { //nolint:gochecknoinits
	conf = &config{}
}

func errPanic(err error) {
	if err != nil {
		panic(err)
	}
}

func errLog(err error) {
	if err != nil {
		log.Printf("%+v", err)
	}
}

func main() {
	log.SetFlags(log.LstdFlags | log.Lshortfile)

	var err error

	errPanic(readConfig())
	errPanic(conf.parse())

	client, err = mautrix.NewClient(conf.hsURI, conf.userID, conf.AccessToken)
	errPanic(err)

	if client.AccessToken == "" {
		_, err = client.Login(&mautrix.ReqLogin{
			Type: mautrix.AuthTypePassword,
			Identifier: mautrix.UserIdentifier{
				Type: mautrix.IdentifierTypeUser,
				User: conf.username,
			},
			Password:         conf.Password,
			StoreCredentials: true,
		})
		errPanic(err)

		log.Printf("accessToken: %s", client.AccessToken)
	}

	store = mautrix.NewInMemoryStore()

	event.TypeMap[EventPollResponse] = reflect.TypeOf(PollResponseContent{})
	event.TypeMap[EventPollStart] = reflect.TypeOf(PollStartContent{})
	event.TypeMap[EventPollEnd] = reflect.TypeOf(PollEndContent{})

	syncer, ok := client.Syncer.(*mautrix.DefaultSyncer)
	if !ok {
		log.Printf("not DefaultSyncer")
	}

	syncer.ParseErrorHandler = func(e *event.Event, err error) bool {
		log.Printf("unknown type %+v", e)

		return false
	}

	syncer.OnSync(func(resp *mautrix.RespSync, since string) bool {
		for roomID, evts := range resp.Rooms.Join {
			room := store.LoadRoom(roomID)
			if room == nil {
				room = mautrix.NewRoom(roomID)
				store.SaveRoom(room)
			}
			for _, i := range evts.State.Events {
				room.UpdateState(i)
			}
			for _, i := range evts.Timeline.Events {
				if i.Type.IsState() {
					room.UpdateState(i)
				}
			}
		}

		return true
	})
	syncer.OnEvent(func(source mautrix.EventSource, e *event.Event) {
		if source&mautrix.EventSourceJoin != 0 && source&mautrix.EventSourceTimeline != 0 {
			errPanic(client.MarkRead(e.RoomID, e.ID))
		}
	})
	syncer.OnEventType(event.StateMember, func(source mautrix.EventSource, e *event.Event) {
		if e.Content.AsMember().Membership != event.MembershipInvite {
			return
		}

		if e.GetStateKey() != conf.UserID {
			return
		}

		_, err := client.JoinRoomByID(e.RoomID)
		errLog(err)

		processMenu(e)
	})
	syncer.OnEventType(event.EventMessage, func(source mautrix.EventSource, e *event.Event) {
		if e.Sender == conf.userID {
			return
		}

		message := e.Content.AsMessage()
		if message.MsgType == event.MsgNotice {
			return
		}
		body := strings.TrimSpace(message.Body)
		log.Printf("%s: %s", e.Sender, body)

		fields := strings.Fields(body)
		if strings.HasPrefix(fields[0], "!") {
			processCommand(e, fields, body)
		}
	})
	syncer.OnEventType(EventPollResponse, func(source mautrix.EventSource, e *event.Event) {
		content := PollEndContent{
			PollEnd:      map[string]string{},
			MsgType:      event.MsgText,
			FallbackText: "poll ended",
			Body:         "poll ended",
			RelatesTo: &event.RelatesTo{
				EventID: e.Content.Parsed.(*PollResponseContent).RelatesTo.EventID,
				Type:    event.RelReference,
			},
		}

		_, err := client.SendMessageEvent(e.RoomID, EventPollEnd, content)
		errLog(err)

		switch e.Content.Parsed.(*PollResponseContent).PollResponse.Answers[0] {
		case "door":
			content := PollStartContent{
				PollStart: PollStart{
					Question: PollQuestion{
						Text:    "do you want to open or close the door?",
						Body:    "do you want to open or close the door?",
						MsgType: event.MsgText,
					},
					Kind:          KindDisclosed,
					MaxSelections: 1,
					Answers: []PollAnswer{
						{
							ID:   "close",
							Text: "close",
						},
						{
							ID:   "open",
							Text: "open",
						},
					},
				},
			}
			_, err := client.SendMessageEvent(e.RoomID, EventPollStart, content)
			errLog(err)
		case "light":
			content := PollStartContent{
				PollStart: PollStart{
					Question: PollQuestion{
						Text:    "do you want to turn on or off the light?",
						Body:    "do you want to turn on or off the light?",
						MsgType: event.MsgText,
					},
					Kind:          KindDisclosed,
					MaxSelections: 1,
					Answers: []PollAnswer{
						{
							ID:   "turn on",
							Text: "turn on",
						},
						{
							ID:   "turn off",
							Text: "turn off",
						},
					},
				},
			}
			_, err := client.SendMessageEvent(e.RoomID, EventPollStart, content)
			errLog(err)
		case "open":
		case "close":
		case "turn on":
		case "turn off":
		default:
			log.Println("unknown")
		}
	})

	eventIgnorer := mautrix.OldEventIgnorer{UserID: conf.userID}
	eventIgnorer.Register(syncer)

	errPanic(client.Sync())
}
