.PHONY: help
help:
	@cat Makefile

.PHONY: build
build: matrix-menu-bot

matrix-menu-bot: *.go
	CGO_ENABLED=0 GOOS=$$GOOS GOARCH=$$GOARCH go build -trimpath -ldflags "-s -w"

.PHONY: run
run: matrix-menu-bot
	./matrix-menu-bot

.PHONY: docker
docker:
	docker build -t matrix-menu-bot .

.PHONY: dockerx
dockerx:
	DOCKER_BUILDKIT=1 docker buildx build -f Dockerfile.buildx --platform linux/arm64,linux/amd64 -t registry.gitlab.com/ptman/matrix-menu-bot --push .

.PHONY: lint
lint:
	CGO_ENABLED=0 golangci-lint run --enable-all --disable golint,maligned,scopelint,interfacer,wrapcheck,gomoddirectives .

.PHONY: clean
clean:
	rm -f matrix-menu-bot
