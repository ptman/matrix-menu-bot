# Menu driven matrix bot interaction

I had high hopes for [MSC3006: Bot
Interaction](https://github.com/matrix-org/matrix-spec-proposals/pull/3006) in
Matrix, but since that is now dropped I'm looking for alternatives. Early poll
bots used reactions for voting. But then there's [MSC3381:
Polls](https://github.com/matrix-org/matrix-spec-proposals/pull/3381) with
client support in Element.

I realized that while reactions can be used for interacting with bots, polls are
also an alternative. Here's a little bot that presents a simple menu using
polls.

## How does it work?

1. You join a room with the bot
2. The bot presents you with a list of options as a poll
3. After you select an option, the bot closes the poll and may execute further
   actions or present you with a new selection.
4. There is no step four.

It's a simple menu driven interface. Like dialog(1), whiptail(1) or zenity(1).

## Demo

Open a DM with [@menubot:matrix.org](matrix:u/menubot:matrix.org)

## Blog post

I wrote about this project on [my
blog](https://paul.totterman.name/posts/matrix-menu-bot/).

## Contact

I'm [@ptman:kapsi.fi](matrix:u/ptman:kapsi.fi)

## License

ISC
